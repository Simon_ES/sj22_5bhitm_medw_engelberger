<?php

$servername = "localhost";
$username = "medt4";
$password = "medt4";

try {
    $conn = new PDO("mysql:host=$servername;dbname=medt5", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$stmt = $conn->prepare("select * from provider_comparison");
$stmt->execute();

$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Strompreisvergleich</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous"></script>
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Strompreisvergleich</h1>
            <form method="POST">
                <div class="input-group mb-3 mt-2">
                    <span class="input-group-text">kWh/Jahr</span>
                    <div class="form-floating">
                        <input type="text" class="form-control"placeholder="kWh/Jahr" name="kwh">
                        <label for="kWhInputGroup">Stromverbrauch</label>
                    </div>
                    <button type="submit" name="submit" class="btn btn-dark">Jetzt vergleichen</button>
                </div>
            </form>
        </div>
    </div>
    <?php
    if(isset($_POST['kwh'])  && !empty(trim($_POST['kwh']))){
        echo '<div class="row">
        <div class="col">
            <hr>
            <h2>3 Angebote</h2>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Anbieter</th>
                    <th scope="col">Wichtige Information</th>
                    <th scope="col">Jahrespreis</th>
                </tr>
                </thead>
                <tbody>';

        $kwh = $_POST['kwh'];
        $case = '';

        switch($kwh){
            case $kwh <= 1000:
                $case = 'price1';
                break;
            case $kwh <= 3000:
                $case = 'price2';
                break;
            case $kwh > 3000:
                $case = 'price3';
                break;
            default:
                alert("Bitte Zahl eingeben");
        }

        foreach($result as $provider){
            echo "<tr>
                        <th scope='row'>". $provider['id'] ."</th>
                        <td>". $provider['provider'] ."</td>
                        <td>". $provider['information'] ."</td>
                        <td>€ ". $provider[$case] * $kwh / 100 ."</td>
                    </tr>";
        }

        echo "</tbody>
            </table>
        </div>
    </div>";
    }
    ?>

</div>


</div>

</body>
</html>