<?php

$students = [[0, "Martin Bauer", "True"],
    [1, "Josef Dorfer", "True"],
    [2, "Caro Feiersinger", "True"],
    [3, "Clemens Waldhör", "False"],
    [4, "Anita Felsner", "True"],
    [5, "Susanne Korn", "True"],
    [6, "Wolfgang Gartner", "True"],
    [7, "Tom Turbo", "True"],
    [8, "Daniel Maier", "True"],
    [9, "Lukas Fellner", "True"],
    [10, "Benedikt Huber", "True"],
    [11, "Anton Polster", "True"],
    [12, "Sandra Pomassel", "True"],
    [13, "Willi Zauner", "True"],
    [14, "Mario Durnwalder", "True"],
    [15, "Martin Thurner", "True"],
    [16, "Markus Sommer", "True"],
    [17, "Gerald Widhalm", "True"],
    [18, "Martina Fasching", "False"],
    [19, "Johanna Schiebel", "False"],
    [20, "Julia Wagner", "False"],
    [21, "Egon Braunschweig", "False"]];

    if(isset($_POST['id'])){
        echo json_encode($_POST['id']);
        exit;
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>Randomizer</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="https://csshake.surge.sh/csshake.min.css">
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<div class="container">
    <div class="row mt-3">
        <div class="col-md-5">
            <h1>5 BHITM</h1>
        </div>
        <div class="col">
            <button type="button" class="btn btn-danger btn-lg" id="chooseStudent">Lets go >>></button>
        </div>
    </div>
    <div class="row mt-3">
        <?php
            foreach($students as $student){
                echo "
                <div class='col-md-4 mt-2'>
                <div class='card'>
                    <div class='card-body'>". $student[1] ."
                    <hr>
                    <button class='btn changeState' id='". $student[0] ."'>Change true/false</button>
                    </div>
                </div></div>";
            }

        ?>
    </div>
</div>

<script>

    $(".changeState").click(function (e){
        let studentId = this.id
        console.log(studentId)
        $.ajax({
            type : "post",
            data : {id : studentId},
            dataType : 'json',
            success : function (response){
                alert($('#' + response).text());
            }
        })
    })

</script>

</body>
</html>