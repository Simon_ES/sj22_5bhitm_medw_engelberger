<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Editieren</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <?php

    if (isset($_POST["edit"])) {

        $id = $_POST["edit"];

        $servername = "localhost";
        $username = "medt4";
        $password = "medt4";

        $conn = new PDO("mysql:host=$servername;dbname=guestbook", $username, $password);

        $sql = $conn->prepare("select * from entries where Entry_ID = :id");
        $sql->bindParam(":id", $id);
        $sql->execute();
        $result = $sql->fetchObject();

        echo "<form method='post' action='edit.php'>
        <label>Message</label>
        <input type='text' value='" . $result->Message . "' name='message'>
        <button type='submit' name='submit' value='". $_POST["edit"] ."' class='btn btn-primary'>Edit</button>
    </form>";
    } else {
        echo 'no message found';
    }
    ?>
</div>

<?php

if(isset($_POST["message"])){
    edit_data($_POST["submit"], $_POST["message"]);
}

function edit_data($id, $message){
    $servername = "localhost";
    $username = "medt4";
    $password = "medt4";

    $conn = new PDO("mysql:host=$servername;dbname=guestbook", $username, $password);

    $sql = $conn->prepare("UPDATE entries SET Message=:message WHERE Entry_ID = :id");
    $sql->bindParam(":message", $message);
    $sql->bindParam(":id", $id);
    $sql->execute();

    header("Location: index.php");
    exit();
}

?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
</body>
</html>