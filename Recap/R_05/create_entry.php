<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Eintrag erstellen</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="">Beitrag erstellen</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Übersicht</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid col-md-offset-3 col-md-6 mt-5">
    <form method="post" action="create_entry.php">
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" name="name" required>
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input type="email" class="form-control" name="email" required>
        </div>
        <div class="mb-3">
            <label for="message" class="form-label">Nachricht</label>
            <input type="text" class="form-control" name="message" required>
        </div>
        <button type="submit" name="submit" class="btn btn-primary">Speichern</button>
    </form>
</div>


<?php

    if(isset($_POST["submit"])){
        create_entry();
    }

    function create_entry(){

        $servername = "localhost";
        $username = "medt4";
        $password = "medt4";

        $conn = new PDO("mysql:host=$servername;dbname=guestbook", $username, $password);

        date_default_timezone_set('Europe/Berlin');

        $data = [
                "date" => date('Y-m-d', time()),
                'name' => $_POST["name"],
                'email' => $_POST["email"],
                'message' => $_POST["message"]
        ];

        $sql = "INSERT INTO entries (name, date, email, message) VALUES (:name, :date, :email, :message)";
        $stmt= $conn->prepare($sql);
        $stmt->execute($data);
    }
?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
</body>
</html>