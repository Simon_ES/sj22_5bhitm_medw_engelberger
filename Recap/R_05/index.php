<?php
$servername = "localhost";
$username = "medt4";
$password = "medt4";

$conn = new PDO("mysql:host=$servername;dbname=guestbook", $username, $password);

$stmt = $conn->prepare("SELECT * from entries order by date desc;");
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Übersicht</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="">Übersicht</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="create_entry.php">Beitrag erstellen</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<?php
    foreach ($result as $entry){
        echo "<div class='container-fluid col-md-offset-3 col-md-6 mt-5'>
    <div class='card' style='mb-6'>
        <div class='card-body'>
            <h5 class='card-title'>". $entry['Name'] ."</h5>
            <h6 class='card-subtitle mb-2 text-body-secondary'>". $entry['Email'] ."</h6>
            <p class='card-text'>". $entry['Message'] ."</p>
        </div>
        <div class='card-footer'>
    ". $entry['Date'] ."
    <form method='post' action='index.php'>
    <button type='submit' name='delete' value='". $entry['Entry_ID'] ."' class='btn btn-primary'>Delete</button>
</form>
<br>
<form method='post' action='edit.php'>
    <button type='submit' name='edit' value='". $entry['Entry_ID'] ."' class='btn btn-primary'>Edit</button>
</form>
  </div>
    </div>
</div>";
    }
?>

<?php

if(isset($_POST["delete"])){
    delete_entry($_POST["delete"]);
}

function delete_entry($id){

    $servername = "localhost";
    $username = "medt4";
    $password = "medt4";

    $conn = new PDO("mysql:host=$servername;dbname=guestbook", $username, $password);

    $sql = $conn->prepare("DELETE FROM entries where Entry_ID = :id");
    $sql->bindParam(":id", $id);

    $sql->execute();
}

?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
</body>
</html>