<footer>
    <div class="site-info">
        <?php printf( esc_html__( 'Copyright © %1$s %2$s', 'yourtheme' ),
            date_i18n( 'Y' ), esc_html__( get_bloginfo( 'name' ), 'yourtheme' ) ); ?>
    </div>
    <div class="footer-text">
        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
            Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
            dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et
            justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
            sanctus est Lorem ipsum dolor sit amet.</p>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>