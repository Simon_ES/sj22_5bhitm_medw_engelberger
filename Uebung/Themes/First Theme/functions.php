<?php

function yourtheme_enqueue_styles() {
    wp_enqueue_style( 'yourtheme-style', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'yourtheme_enqueue_styles' );