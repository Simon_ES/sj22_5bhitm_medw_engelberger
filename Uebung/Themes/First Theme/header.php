<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
    <link rel="stylesheet" href="style.css">
</head>
<body <?php body_class(); ?>>
<header>
    <div class="site-title">
        <h1>This is my Header</h1>
    </div>
</header>