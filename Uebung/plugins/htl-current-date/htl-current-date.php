<?php
/**
 * Plugin Name: HTL Current Date
 * Description: Amazing Plugin.
 * Version: 1.0
 * Author: IT Kaufmann
 */

/**
 * Hooks im Allgemeinen und Filter Hooks im speziellen bilden die Schnitstelle
 * zum Wordpress-Core. Dieser Hook wird bei jedem Click im Frontend ausgeführt.
 * @param: the_content enthält den jeweiligen Page/Post-Inhalt.
 * @param: addDateToEndOfThePost die Funktion, die es auszuführen gilt.
 */
add_filter('the_content','addDateToEndOfThePost');

function addDateToEndOfThePost($content){
    return $content . "<p>".date('d.m.Y')."</p>";
}