<?php

/**
 * Plugin Name: Shortcode Form
 * Description: ShortCode Form
 * Version: 1.0
 * Author: HTL Super Coder
 */

class HtlForm {


    public function __construct() {
        $this->InsertData();
        add_action('admin_menu', array($this, 'pluginSettingMenuEntry'));
        add_action('admin_init', array($this, 'settings'));
        add_shortcode( 'plugin_form', array($this, 'generateForm'));
    }

    function settings() {
        add_settings_section('fp_first_section', null, null, 'form-settings-page');
    }

    function pluginSettingMenuEntry() {
        add_options_page('Form settings', 'Form', 'manage_options', 'form_plugin', array($this, 'pluginSettingHTML'));
    }

    function pluginSettingHTML() {
        global $wpdb;
        $users = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "plugin_form_users");
        ?>
        <div>
            <h3>Users</h3>
        </div>
        <br>
        <table>
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Mail</th>
                <th>Newsletter</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $row) { ?>
                <tr>
                    <td><?php echo $row->FIRST_NAME ?></td>
                    <td><?php echo $row->LAST_NAME ?></td>
                    <td><?php echo $row->EMAIL ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php }

    function outputForm() {
        if (is_main_query()) {
            ?>
            <h3 class="lead">Anmelde-Formular</h3>
            <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" class="px-5 pt-5 pb-3 mb-5 border">
                <input name='action' type="hidden" value='custom_form_submit'>
                <div class="form-group">
                    <label>Vorname</label>
                    <input type="text" name="first_name" class="form-control" required placeholder="Max">
                    <label>Nachname</label>
                    <input type="text" name="last_name" class="form-control" required placeholder="Mustermann">
                    <label>E-Mail</label>
                    <input type="text" name="email" class="form-control" required placeholder="muster@htlkrems.at">
                    <div class="form-check">
                        <input type="checkbox" name="newsletter_abo" class="form-check-input">
                        <label>Newsletter</label>
                    </div>
                </div>
                <input type="hidden" name="action" value="contact_form">
                <button class="btn btn-secondary" type="submit">Abschicken</button>
            </form>
            <?php
        } else { ?>
            <p>Anmeldung vorübergehen nicht möglich.</p>
        <?php }
    }
    function generateForm($content){
        $html = '<form action="# " method="post">
        <input name="action" type="hidden" value="custom_form_submit">
                <div>
                    <div>
                    <label>Vorname</label>
                    <input type="text" name="first_name" required placeholder="First Name">
                    </div>
                    <div>
                    <label>Nachname</label>
                    <input type="text" name="last_name" required placeholder="Last Name">
                    </div>
                    <div>
                    <label>E-Mail</label>
                    <input type="text" name="email" required placeholder="Email">
                    </div>
                    <div>
                        <input type="checkbox" name="news">
                        <label>Newsletter</label>
                    </div>
                </div>
                <input type="hidden" name="action" value="contact_form">
                <button class="btn" type="submit">Submit</button>
                </form>';

        return $content.$html;
    }

    function InsertData(){
        global $wpdb;

        if(isset($_POST["first_name"]) and $_POST["last_name"] and $_POST["email"]!=""){
            $table_name = $wpdb->prefix . "plugin_form_users";

            $first_name = $_POST["first_name"];
            $last_name = $_POST["last_name"];
            $email = $_POST["email"];

            $newsletter = 0;
            if(isset($_POST["news"])){
                $newsletter = 1;
            }

            $data = array('FIRST_NAME' => $first_name, 'LAST_NAME' => $last_name, "EMAIL" => $email, 'NEWSLETTER' => $newsletter);

            $wpdb->insert( $table_name, $data);
        }
    }

}
new HtlForm();