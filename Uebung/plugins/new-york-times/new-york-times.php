<?php
/**
 * Plugin Name: New York Times
 * Description: gibt New York Times Artikel aus
 * Version: 0.1
 * Author: HTL Kranker Chef
 */

class NewYork
{
    function __construct(){
        add_action('admin_menu', array($this, 'pluginSettingMenuEntry'));
        add_action('admin_init', array($this, 'settings'));
        add_filter('the_content', array($this, 'outputNewYork'));
    }

    function settings(){
        //add_settings_section( string $id, string $title, callable $callback, string $page )
        add_settings_section('nyt_first_section', null, null, 'NewYork-settings-page');
        //add_settings_field( string $id, string $title, callable $callback, string $page, string $section = 'default', array $args = array() )
        add_settings_field('nyt_location', 'Article Amount', array($this, 'locationHTML'), 'NewYork-settings-page', 'nyt_first_section');
        //register_setting( string $option_group, string $option_name, array $args = array() )
        register_setting('newyork_plugin', 'nyt_location', array('sanitize_callback' => 'sanitize_text_field', 'default'=>1));
        //add_settings_field( string $id, string $title, callable $callback, string $page, string $section = 'default', array $args = array() )
        add_settings_field('nyt_header', 'Header:', array($this, 'TagHTML'), 'NewYork-settings-page', 'nyt_first_section');
        //register_setting( string $option_group, string $option_name, array $args = array() )
        register_setting('newyork_plugin', 'nyt_header', array('sanitize_callback' => 'sanitize_text_field', 'default'=>"SIUU"));
    }

    function pluginSettingMenuEntry(){
        add_options_page('NewYork Settings', 'NewYork', 'manage_options', 'newyork_plugin', array($this, 'pluginSettingHTML'));
    }

    function pluginSettingHTML(){ ?>
        <div class='wrap'><h1>NewYork Settings</h1>
            <form action="options.php" method="post">
                <?php
                settings_fields('newyork_plugin');
                do_settings_sections('NewYork-settings-page');
                submit_button();
                ?>
            </form>
        </div>
    <?php }

    function outputNewYork($content){
        $file = '{
            "posts" : [
             {
               "title": "sit dolor magna voluptate",
               "content": "sit",
               "source": {
                 "name": "mollit esse in enim",
                 "url": "https://Br.swetrK3XkjN7.7P4kulviTKKqMN,aWTqxvf1lQMVsHl3J,Qbi9"
               },
               "image": "http://kpqf.goskog.gNZToO,ffmRopLhinO67+QYiucrD+-epAFDvjJ,uv,M",
               "keywords": [
                 "cillum fugiat consequat est"
               ],
               "publishDate": "1976-09-30T13:53:19.0Z",
               "summary": "nisi",
               "url": "https://tFLsYAXs.crckMfA3d1wbpwAEcgnZMCmRTXvFXgbiD",
               "category": "proident",
               "author": "dolor exercitation sit"
             },
             {
               "title": "voluptate aute",
               "content": "dolor irure est esse magna",
               "source": {
                 "name": "labore dolor",
                 "url": "http://NuVPHveARpeNalXGXsASocDdQ.jlrMgc+BvTyiLP4Oek1iCVeu,wKPDBlv8dXkD-iYXI3E3b"
               },
               "image": "https://AKztWtmktNzFjkDXPcmYj.hnWMkSW0+EAJcDaV4tUsNthZOE3xHRKZUQGNtAouiQstTvkQnRrRnqmx.EMqCmAdupUoZzmkF3",
               "author": "commodo cillum do Ut",
               "publishDate": "1995-09-14T20:31:45.0Z"
             },
             {
               "title": "enim non",
               "content": "elit ipsum est cupidatat",
               "source": {
                 "name": "reprehenderit non",
                 "url": "http://uxNEgiKdRSFSeavypzRAJABkmgt.vbkVG"
               },
               "publishDate": "1943-08-16T23:01:58.0Z",
               "url": "http://KWllPwURohytyLdtgSQP.cmpNTg8VItlcfs5mXUubKB5O4GeWplCB0IrLbaUI8IKoMN,aD7n",
               "category": "laboris in in irure",
               "image": "https://jTYBrwCjRrseZlRnbnGj.iyW3Whs4vfde9HzWnIzeTvBj.CfRqI4StK",
               "author": "sunt enim in",
               "summary": "ea",
               "keywords": [
                 "mollit nostrud occaecat proident irure",
                 "adipisicing nulla nostrud"
               ]
             },
             {
               "title": "Duis ut elit",
               "content": "in exercitation velit",
               "source": {
                 "name": "magna eu",
                 "url": "http://SgjmwLBziJGRYfDL.ryZkWMqIT7Vj5jb60KnhdeV9CeOJ2oNx"
               },
               "author": "deserunt elit consectetur",
               "url": "https://OQcEGQfhGG.thyX9bXqb3i9Qr"
             },
             {
               "title": "in",
               "content": "ea",
               "source": {
                 "name": "elit nostrud qui minim dolore",
                 "url": "http://QuEaYOSfKFSywpuUzQldfhxjD.ztxM1CCcXP8TRGhpPf3Gln4qAFJTu-0NrLFH16+iWezw"
               },
               "keywords": [
                 "dolore occaecat magna irure ad",
                 "nulla",
                 "deserunt",
                 "ad veniam eu Excepteur"
               ],
               "author": "enim",
               "summary": "nostrud",
               "url": "http://hYoKOxSrkWvbNgYujXDALfaLGssHEW.owK6AkFEnti9Rz81SIjbMIGigntXBL4Thd4pq9-SnhEYXv4EDrebH"
             }
           ]
           }';
        $jsonArray = json_decode($file, true);

        $text="<h1>".get_option('nyt_header')."</h1><ul>";
        $i=0;
        foreach($jsonArray["posts"] as $item) if($i<get_option('nyt_location')){

            $text.="<li>";
            $text.=$item["title"];
            $text.="</li>";
            $i++;
        }
        $text.="</ul>";

        return $content.=$text;
    }

    function locationHTML()
    {
        ?>
        <input type="number" name="nyt_location" value="<?php echo get_option('nyt_location');?>" min=1 max=5>
    <?php }

    function TagHTML()
    {
        ?>
        <input type="text" name="nyt_header" value="<?php echo get_option('nyt_header');?>">
    <?php }
}
new NewYork();


?>
