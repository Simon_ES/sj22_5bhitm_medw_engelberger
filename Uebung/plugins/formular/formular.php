<?php

/**
 * Plugin Name: Form Plugin
 * Description: Show  with a Schorcode [plugin_form] a form with validation for (Firstname, Lastname, Email and Newsletter) and save the inputs in a table
 * Version: 1.0
 * Author: Someone
 */

class FormPlugin{

    function __construct()
    {
        register_activation_hook( __FILE__, array($this, 'createTable'));
        add_action('admin_menu', array($this, 'pluginsSettingMenuEntry'));
        add_action('admin_init', array($this, 'settings'));
        //add_filter('the_content',array($this, 'outputStats'));
        add_shortcode( 'plugin_for',array($this, 'outputForm') );
    }

    function pluginsSettingMenuEntry()
    {
        add_options_page('Form Settings', 'Form Plugin', 'manage_options', 'form_plugin', array($this, 'pluginSettingHTML'));
    }

    function pluginSettingHTML()
    {
        ?>
        <div class='wrap'><h1>Form Plugin Settings</h1>
            <form action="options-general.php?page=form_plugin" method="post">
                <button type="submit" name="show_table">Show registered Users</button>
            </form>
            <?php
            if(isset($_POST['show_table'])){
                global $wpdb;
                $users = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."plugin_form_users;");
                echo "
                    <style>
                    table, th, td {
                      border: 1px solid black;
                      border-collapse: collapse;
                    }
                    th, td {
                      padding-top: 10px;
                      padding-bottom: 10px;
                      padding-left: 15px;
                      padding-right: 15px;
                    }
                    </style>
                    <div style='margin-left: 30%; margin-top: 5%'>
                    <table>
                        <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th>Subscribed</th>
                        </tr>";
                foreach ($users as $user){
                    echo "
                        <tr>
                            <td style='text-align: center'>".$user->FIRST_NAME."</td>
                            <td style='text-align: center'>".$user->LAST_NAME."</td>
                            <td>".$user->EMAIL."</td>";
                    if($user->NEWSLETTER == 0){
                        echo "<td style='text-align: center'>Yes</td>";
                    }
                    else{
                        echo "<td style='text-align: center'>No</td>";
                    }
                    echo "</tr>";
                }
                echo "</table></div>";
            }
            ?>
        </div>
        <?php
    }

    function settings()
    {
        add_settings_section('fp_first_section', null, null, 'form-plugin-page');
    }

    function createTable(){
        global $wpdb;
        $table_name = $wpdb->prefix . "plugin_form_users";
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
              ID INT NOT NULL AUTO_INCREMENT,
              FIRST_NAME varchar(255) null,
              LAST_NAME  varchar(255) null,
              EMAIL      varchar(255) null,
              NEWSLETTER tinyint      null
              PRIMARY KEY  (ID)
            ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
}

new FormPlugin();