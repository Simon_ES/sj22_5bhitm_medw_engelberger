<?php
/**
 * Plugin Name: Post Statistics
 * Description: Another amazing plugin.
 * Version: 1.0
 * Author: HTL Super Coder
 */

class PostStat
{
    function __construct(){
        add_action('admin_menu', array($this, 'pluginSettingMenuEntry'));
        add_action('admin_init', array($this, 'settings'));
    }

    function settings(){
        //add_settings_section( string $id, string $title, callable $callback, string $page )
        add_settings_section('psp_first_section', null, null, 'post-stat-settings-page');
        //add_settings_field( string $id, string $title, callable $callback, string $page, string $section = 'default', array $args = array() )
        add_settings_field('psp_location', 'Display Location', array($this, 'locationHTML'), 'post-stat-settings-page', 'psp_first_section');
        //register_setting( string $option_group, string $option_name, array $args = array() )
        register_setting('post_stat_plugin', 'psp_location', array('sanitize_callback' => 'sanitize_text_field', 'default'=>0));
    }

    function pluginSettingMenuEntry(){
        add_options_page('Post Stat Settings', 'Post Stat', 'manage_options', 'post_stat_plugin', array($this, 'pluginSettingHTML'));
    }

    function pluginSettingHTML(){ ?>
        <div class='wrap'><h1>Post Stat Settings</h1>
            <form action="options.php" method="post">
                <?php
                settings_fields('post_stat_plugin');
                do_settings_sections('post-stat-settings-page');
                submit_button();
                ?>
            </form>
        </div>
    <?php }

    function locationHTML(){?>
        <select name="psp_location">
            <option value="0" <?php selected(get_option('psp_location'), '0') ?>>Beginning of post</option>
            <option value="1" <?php selected(get_option('psp_location'), '1') ?>>End of post</option>
        </select>
    <?php }

}
new PostStat();