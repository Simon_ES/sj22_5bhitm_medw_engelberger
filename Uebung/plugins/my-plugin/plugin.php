<?php
/**
 * Hello World
 *
 * @package HelloWorld
 * @author Semper
 * @copyright 2020 Your Name
 * @license GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: Viech
 * Plugin URI: https://mysite.com/hello-world
 * Description: Prints "Hello World" in WordPress admin.
 * Version: 0.0.1
 * Author: Your Name
 * Author URI: https://mysite.com
 * Text Domain: hello-world
 * License: GPL v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt */

/**
 * [current_year] returns the Current Year as a 4-digit string.
 * @return string Current Year
 */
add_shortcode( 'current_year', 'salcodes_year' );
function salcodes_init(){
	function salcodes_year() {
		return getdate()['year'];
	}
}
add_action('init', 'salcodes_init');

//Button
add_shortcode( 'cta_button', 'salcodes_cta' );

function salcodes_cta( $atts ) {
	$a = shortcode_atts( array(
		'link' => '#',
		'id' => 'salcodes',
		'color' => 'blue',
		'size' => '',
		'label' => 'Button',
		'target' => '_self'
	), $atts );
	$output = '<p><a href="' . esc_url( $a['link'] ) . '" id="' . esc_attr( $a['id'] ) . '" class="button ' . esc_attr( $a['color'] ) . ' ' . esc_attr( $a['size'] ) . '" target="' . esc_attr($a['target']) . '">' . esc_attr( $a['label'] ) . '</a></p>';
	return $output;
}

/** Enqueuing the Stylesheet for the CTA Button */

function salcodes_enqueue_scripts() {
	global $post;
	if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'cta_button') ) {
		wp_register_style( 'salcodes-stylesheet',  plugin_dir_url( __FILE__ ) . 'css/style.css' );
		wp_enqueue_style( 'salcodes-stylesheet' );
	}
}
add_action( 'wp_enqueue_scripts', 'salcodes_enqueue_scripts');

/**
 * [boxed] returns the HTML code for a content box with colored titles.
 * @return string HTML code for boxed content
 */

add_shortcode( 'boxed', 'salcodes_boxed' );

function salcodes_boxed( $atts, $content = null, $tag = '' ) {
	$a = shortcode_atts( array(
		'title' => 'Title',
		'title_color' => 'white',
		'color' => 'blue',
	), $atts );

	$output = '<div class="salcodes-boxed" style="border:2px solid ' . esc_attr( $a['color'] ) . ';">'.'<div class="salcodes-boxed-title" style="background-color:' . esc_attr( $a['color'] ) . ';"><h3 style="color:' . esc_attr( $a['title_color'] ) . ';">' . esc_attr( $a['title'] ) . '</h3></div>'.'<div class="salcodes-boxed-content"><p>' . esc_attr( $content ) . '</p></div>'.'</div>';

	return $output;
}
?>