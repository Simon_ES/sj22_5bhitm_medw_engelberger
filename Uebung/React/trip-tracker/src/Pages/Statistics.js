import React from 'react';
import { Card, ListGroup, ListGroupItem, Container, Col, Row } from 'react-bootstrap';

function Statistics({ data }) {

    const totalTrips = data.length;

    const totalKilometers = data.reduce((sum, trip) => {
        const tripKilometers = parseFloat(trip.endingMileage) - parseFloat(trip.startingMileage);
        return sum + tripKilometers;
    }, 0);

    const averageKilometers = totalKilometers / data.length;

    const averagePassengers = data.reduce((sum, trip) => {
        const passengerNames = trip.passengers.split(' ');
        const numberOfPassengers = passengerNames.filter(name => name.trim() !== '').length;
        return sum + numberOfPassengers;
    }, 0) / data.length;


    return (

        <Container className="mt-5">
            <Row className="d-flex flex-column align-items-center justify-content-center">
                <Col xs={12} sm={6} md={4} lg={4} className="mb-4">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header>Statistics</Card.Header>
                        <ListGroup variant="flush">
                            <ListGroupItem>Total Trips: {totalTrips}</ListGroupItem>
                            <ListGroupItem>Total Kilometres: {totalKilometers.toFixed(2)} km</ListGroupItem>
                            <ListGroupItem>Average Kilometres per trip: {averageKilometers.toFixed(2)} km</ListGroupItem>
                            <ListGroupItem>Average number of passengers per trip: {averagePassengers.toFixed(2)}</ListGroupItem>
                        </ListGroup>
                    </Card>
                </Col>
            </Row>
        </Container>

    );
}

export default Statistics;