import React, { useState } from 'react';
import { Row, Col, Container, Table } from 'react-bootstrap';

function DataTable({ data }) {

    const [filter, setFilter] = useState('');
    const [sortColumn, setSortColumn] = useState("startingLocation");
    const [sortDirection, setSortDirection] = useState('desc');
    
    /*
    const handleFilterChange = (event) => {
        setFilter(event.target.value);
    };
    */

    const sortData = (a, b) => {

        console.log('sortData called with:', { a, b, sortColumn, sortDirection });

        if (sortDirection === 'asc') {
            if (a[sortColumn] < b[sortColumn]) {
                return -1;
            }
            if (a[sortColumn] > b[sortColumn]) {
                return 1;
            }
            return 0;
        } else {
            if (a[sortColumn] < b[sortColumn]) {
                return 1;
            }
            if (a[sortColumn] > b[sortColumn]) {
                return -1;
            }
            return 0;
        }
    };

    // Sortieränderungen verarbeiten
    const handleSortChange = (column) => {
        if (sortColumn === column) {
            setSortDirection(sortDirection === 'asc' ? 'desc' : 'asc');
        } else {
            setSortColumn(column);
            setSortDirection('asc');
        }
    };

    return (
        <>

            <Container className='mt-2'>
                <Row>
                    <Col lg={8} md={10} sm={12} className="mx-auto">
                        <h1>Previous Trips:</h1>
                        <div className='table-responsive'>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th onClick={() => handleSortChange('startingLocation')} >Starting Location</th>
                                        <th onClick={() => handleSortChange('destination')}>Destination</th>
                                        <th onClick={() => handleSortChange('startingDate')}>Starting Date</th>
                                        <th onClick={() => handleSortChange('endingDate')}>Ending Date</th>
                                        <th onClick={() => handleSortChange('startingMileage')}>Starting Mileage</th>
                                        <th onClick={() => handleSortChange('endingMileage')}>Ending Mileage</th>
                                        <th onClick={() => handleSortChange('tripPurpose')}>Purpose of the trip</th>
                                        <th onClick={() => handleSortChange('passenger')}>Passengers</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {data
                                        .filter((entry) =>
                                            Object.values(entry).some((value) =>
                                                value
                                                    .toString()
                                                    .toLowerCase()
                                                    .includes(filter.toLowerCase())
                                            )
                                        )
                                        .sort(sortData)
                                        .map((entry, index) => (
                                            <>
                                                <tr key={index}>
                                                    <td>{entry.startingLocation}</td>
                                                    <td>{entry.destination}</td>
                                                    <td>{entry.startingDate}</td>
                                                    <td>{entry.endingDate}</td>
                                                    <td>{entry.startingMileage}</td>
                                                    <td>{entry.endingMileage}</td>
                                                    <td>{entry.tripPurpose}</td>
                                                    <td>{entry.passengers}</td>
                                                </tr>
                                            </>
                                        ))}

                                </tbody>
                            </Table>
                        </div>
                    </Col>
                </Row>
            </Container>

        </>
    );
}

export default DataTable;