import React, { useState, useEffect } from 'react';
import { Row, Col, Container, Form, Button } from 'react-bootstrap';
import DataTable from './DataTable';
import Statistics from './Statistics';

function TrackingForm() {

    const getEmptyFormValues = () => ({
        startingLocation: '',
        destination: '',
        startingDate: '',
        endingDate: '',
        startingMileage: '',
        endingMileage: '',
        tripPurpose: '',
        passengers: '',
    });

    const [formValues, setFormValues] = useState(getEmptyFormValues());
    const [formDataArray, setFormDataArray] = useState(() => {
        const storedData = localStorage.getItem('formDataArray');
        return storedData ? JSON.parse(storedData) : [];
    });

    const handleSubmit = (event) => {
        event.preventDefault();
        setFormDataArray([...formDataArray, formValues]);
        setFormValues(getEmptyFormValues());
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormValues({ ...formValues, [name]: value });
    };

    useEffect(() => {
        localStorage.setItem('formDataArray', JSON.stringify(formDataArray));
    }, [formDataArray]);

    return (
        <>
            <Container>
                <Row>
                    <Col lg={8} md={10} sm={12} className="mx-auto">
                        <h1>Trip tracker</h1>
                        <Form className='mb-6' onSubmit={handleSubmit}>
                            <Row className='mb-3'>
                                <Form.Group as={Col} controlId='formStartingLocation'>
                                    <Form.Label>Starting Location</Form.Label>
                                    <Form.Control type='text' placeholder='Enter starting location' name='startingLocation'
                                        value={formValues.startingLocation} onChange={handleChange} required/>
                                    <Form.Text className='text-muted'>
                                        Enter the starting location of your trip.
                                    </Form.Text>
                                </Form.Group>

                                <Form.Group as={Col} controlId='formDestination'>
                                    <Form.Label>Destination</Form.Label>
                                    <Form.Control type='text' placeholder='Enter Destination' name='destination'
                                        value={formValues.destination} onChange={handleChange} required/>
                                    <Form.Text className='text-muted'>
                                        Enter the Destination of your trip.
                                    </Form.Text>
                                </Form.Group>
                            </Row>

                            <Row className='mb-3'>
                                <Form.Group as={Col} controlId='formStartingDate'>
                                    <Form.Label>Starting Date</Form.Label>
                                    <Form.Control type='datetime-local' placeholder='Enter starting date' name='startingDate'
                                        value={formValues.startingDate} onChange={handleChange} required/>
                                    <Form.Text className='text-muted'>
                                        Enter the starting date of your trip.
                                    </Form.Text>
                                </Form.Group>

                                <Form.Group as={Col} controlId='formEndingDate'>
                                    <Form.Label>Ending Date</Form.Label>
                                    <Form.Control type='datetime-local' placeholder='Enter ending date' name='endingDate'
                                        value={formValues.endingDate} onChange={handleChange} required/>
                                    <Form.Text className='text-muted'>
                                        Enter the end date of your trip.
                                    </Form.Text>
                                </Form.Group>
                            </Row>

                            <Row className='mb-3'>
                                <Form.Group as={Col} controlId='formStartingMileage'>
                                    <Form.Label>Starting Mileage</Form.Label>
                                    <Form.Control type='number' placeholder='Enter starting mileage' name='startingMileage'
                                        value={formValues.startingMileage} onChange={handleChange} required/>
                                    <Form.Text className='text-muted'>
                                        Enter the starting mileage of your trip.
                                    </Form.Text>
                                </Form.Group>

                                <Form.Group as={Col} controlId='formEndingMileage'>
                                    <Form.Label>Ending Mileage</Form.Label>
                                    <Form.Control type='number' placeholder='Enter ending mileage' name='endingMileage'
                                        value={formValues.endingMileage} onChange={handleChange} required/>
                                    <Form.Text className='text-muted'>
                                        Enter the ending mileage of your trip.
                                    </Form.Text>
                                </Form.Group>
                            </Row>

                            <Row className='mb-3'>
                                <Form.Group as={Col} controlId='formTripPurpose'>
                                    <Form.Label>Purpose of the trip</Form.Label>
                                    <Form.Control type='text' placeholder='Enter Purpose of the drip' name='tripPurpose'
                                        value={formValues.tripPurpose} onChange={handleChange} required/>
                                    <Form.Text className='text-muted'>
                                        Enter the purpose of your trip.
                                    </Form.Text>
                                </Form.Group>

                                <Form.Group as={Col} controlId='formPassengers'>
                                    <Form.Label>Passengers</Form.Label>
                                    <Form.Control type='text' placeholder='Enter passengers' name='passengers'
                                        value={formValues.passengers} onChange={handleChange} />
                                    <Form.Text className='text-muted'>
                                        Enter the passengers that took part at the trip.
                                    </Form.Text>
                                </Form.Group>
                            </Row>

                            <Button variant='primary' type='submit' value="submit">
                                Submit
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>

            <DataTable data={formDataArray} />

            <Statistics data={formDataArray} />
        </>
    );
}

export default TrackingForm;